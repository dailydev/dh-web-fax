package kr.dailyhotel

import grails.transaction.Transactional

import org.apache.commons.logging.LogFactory
import org.codehaus.groovy.grails.commons.GrailsApplication;

@Transactional
class HfsenddataService {

	private static final log = LogFactory.getLog(this)
	
	def grailsApplication
	
	def serviceMethod() {
		
	}
	
	def createFaxDoc(def parameterMap) {
		
//		log.debug("createFaxDoc")
//		def docAbsolutePath = servletContext.getRealPath("/") + "/doc"
		String docAbsolutePath = grailsApplication.config.fax.destination.path
		String sourceFilePath = grailsApplication.config.fax.source.path
		String formFileName = grailsApplication.config.fax.form.filename
		
		String currentDatetime = new Date().format('yyMMddHHmm')
		String fileName = "${currentDatetime}-${parameterMap.reservIdx}-${parameterMap.hotelN}-${parameterMap.userN}.docx"
		
		String openDocFilePath = "${sourceFilePath}/${formFileName}"
		String outDocFilePath = "${docAbsolutePath}/${fileName}"
		
		boolean isSuccess = DocxUtil.modifytoSave(openDocFilePath, outDocFilePath, parameterMap)
			
		return ["isSuccess": isSuccess, "fileName": fileName]
	}
	
	def save(def parameterMap) {
		
		def hfsenddata = new Hfsenddata()
		hfsenddata.properties = parameterMap;
		
		if(hfsenddata.save()) {
			
			return true
		}
		else {
			
			hfsenddata.errors.each {
				
				log.error(it)
			}
			
			return false
		}
	}
	
	private def tempDataMap() {
		
		def tempMap = [
			ReservIdx : "99999999",
			PaymentDate : new Date().format("yyyy/MM/dd").toString(),
			Date : new Date().format("yyyy/MM/dd").toString(),
			HotelName : "결제 테스트 호텔",
			UserName : "dh00",
			Tel : "010-1234-5678",
			BedType : "더블",
			Deposit : "9000",
			Discount : "10000",
			AvailableRoomCount : "1"
		]
		
		return tempMap
	}
}