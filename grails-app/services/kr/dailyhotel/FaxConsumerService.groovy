package kr.dailyhotel

import grails.transaction.Transactional
import groovy.time.TimeCategory

import org.apache.commons.logging.LogFactory

@Transactional
class FaxConsumerService {

	private static final log = LogFactory.getLog(this)

	static rabbitQueue = 'hanafaxQueue'
	
	def grailsApplication
	def hfsenddataService
		
    def serviceMethod() {

    }

    void handleMessage(String textMessage) {
        // handle String message…
		log.debug("handle1: ${textMessage}")
    }

    void handleMessage(Map messageMap) {
		
		// 00시 ~ 8시까지 팩스가 전송되지 않음.
//		def currentDate = new Date()
//		def timezone = TimeZone.getTimeZone("Asia/Tokyo")
//		def currentHour = currentDate.format('H', timezone).toInteger()
//		if (currentHour < 8) {
//			
//			log.debug("This is not service time!")
//			return
//		}
		
        // handle Map message…
		log.debug("handle2: ${messageMap}")
		
		def reservationRecSingle = ReservationRecSingle.get(messageMap.reservIdx)
		def hotel = Hotel.get(reservationRecSingle.hotelIdx)
		
		def parameterMap = [:]
		parameterMap.reservIdx = messageMap.reservIdx
		parameterMap.paymentDate = reservationRecSingle.regDate.format("yyyy-MM-dd")
		parameterMap.checkIn = reservationRecSingle.checkinDate.format("yyyy-MM-dd")
		parameterMap.checkOut = reservationRecSingle.checkoutDate.format("yyyy-MM-dd")

		parameterMap.userN = messageMap.userName
		parameterMap.phoneN = messageMap.phoneNumber
		parameterMap.bedType = reservationRecSingle.roomName
		
		parameterMap.depositP = reservationRecSingle.depositTotal
		parameterMap.saleP = reservationRecSingle.sellingPriceTotal
		
		Date startDate = reservationRecSingle.checkinDate
		Date endDate = reservationRecSingle.checkoutDate
		parameterMap.lengthStay = endDate - startDate;
		
		parameterMap.hotelN = hotel.name
		def availableRooms = 0
		while(startDate < endDate) {
			
			def saleReco = SaleReco.findByRoomidxAndSday(reservationRecSingle.roomIdx, startDate)
			def thisAvailableRooms = saleReco.cap - ReservationRec.countBySaleidx(saleReco.id)
			
			if (saleReco.isBlock && hotel.regionProvinceIdx != 5) {
				
				parameterMap.hotelN = "[하드블락]" + hotel.name
			}
			
			if (availableRooms == 0) {
				
				availableRooms = thisAvailableRooms
			}
			else if (availableRooms > thisAvailableRooms) {
				
				availableRooms = thisAvailableRooms
			}
			
			use (TimeCategory) {
				
				startDate = startDate + 1.day
			}
		}
		parameterMap.availableRoom = availableRooms
		
		def createFaxDoc = hfsenddataService.createFaxDoc(parameterMap)
		
		def isSuccess = false
		if (createFaxDoc.isSuccess) {

			parameterMap.strsenderFaxnum = grailsApplication.config.fax.dailyhotel.number
			parameterMap.strsubject = createFaxDoc.fileName.replaceAll(".docx", "")
			parameterMap.strfilenames = createFaxDoc.fileName
			def millis = new Date().time
			def random10 = new Random().nextInt(10)
			parameterMap.strsid = "${millis}" + "${random10}"
			
			def hfsenddata
			if (hotel.fax1 != "") {
				
				parameterMap.strreceiverFaxnum = hotel.fax1
				isSuccess = hfsenddataService.save(parameterMap)
				hfsenddata = Hfsenddata.findByStrsid(parameterMap.strsid)
				reservationRecSingle.faxidx = hfsenddata.id
				reservationRecSingle.save(flush:true)
				
				if (isSuccess && hotel.fax2 != "") {
					
					parameterMap.strreceiverFaxnum = hotel.fax2
					isSuccess = hfsenddataService.save(parameterMap)
					hfsenddata = Hfsenddata.findByStrsid(parameterMap.strsid)
					reservationRecSingle.fax2idx = hfsenddata.id
					reservationRecSingle.save(flush:true)
				}
			}
			else if (hotel.fax2 != "") {
				
				parameterMap.strreceiverFaxnum = hotel.fax2
				isSuccess = hfsenddataService.save(parameterMap)
				hfsenddata = Hfsenddata.findByStrsid(parameterMap.strsid)
				reservationRecSingle.fax2idx = hfsenddata.id
				reservationRecSingle.save(flush:true)
			}
			else {
				if(hotel.email1 != "" || hotel.email2 != "") {
					isSuccess = true
				}
			}
			
			if (!isSuccess) {
				
				log.err('Fail DB proccess');
				sendMail {
		            to "dev.report@dailyhotel.co.kr"
		            subject "dH-AWS-Fax:Grails"
		            body "Error!"
		        }
			}
		}
		else {
			
			log.err('Fail DB proccess');
			sendMail {
	            to "dev.report@dailyhotel.co.kr"
	            subject "dH-AWS-Fax:Grails"
	            body "Error!"
	        }
		}
    }

    void handleMessage(byte[] byteMessage) {
        // handle byte array message…
		log.debug("handle3")
    }
}
