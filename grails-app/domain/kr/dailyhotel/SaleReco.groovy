package kr.dailyhotel

import org.apache.commons.lang.builder.EqualsBuilder
import org.apache.commons.lang.builder.HashCodeBuilder

class SaleReco implements Serializable {

	Integer hotelidx
	Date sday
	Integer roomidx
	Integer price
	Integer discount
	Date regdate
	Integer cap
	Date checkin
	Date checkout
	Integer deposit
	Integer bedType
	Integer hide
	Integer sellingPrice
	Integer deltaPrice
	Byte siteSpecialCode
	Boolean isBlock
	Byte holdByHotel

	static mapping = {
		id column: "idx"
		version false
	}

	static constraints = {
		cap nullable: true
		checkin nullable: true
		checkout nullable: true
		deltaPrice nullable: true
		siteSpecialCode nullable: true
	}
}
