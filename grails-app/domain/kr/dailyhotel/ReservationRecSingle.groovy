package kr.dailyhotel

class ReservationRecSingle {

	Integer userIdx
	Integer hotelIdx
	Integer roomIdx
	Integer paymentType
	Date checkinDate
	Date checkoutDate
	String kcpno
	String tid
	Integer faxidx
	Integer fax2idx
	Integer bonus
	String roomName
	Integer discountTotal
	Integer depositTotal
	Integer sellingPriceTotal
	Integer deltaPriceTotal
	Byte rating
	Boolean userByHidden
	Boolean isCancel
	String guestName
	String guestPhone
	String guestEmail
	Date regDate
	Date updateDate
	Date cancelDate

	static hasMany = [reservationRecs: ReservationRec]

	static mapping = {
		id column: "idx"
		version false
	}

	static constraints = {
		kcpno nullable: true, maxSize: 50
		tid nullable: true, maxSize: 45
		faxidx nullable: true
		fax2idx nullable: true
		roomName nullable: true, maxSize: 45
		discountTotal nullable: true
		depositTotal nullable: true
		rating nullable: true
		guestName maxSize: 45
		guestPhone maxSize: 20
		guestEmail maxSize: 200
	}
}
