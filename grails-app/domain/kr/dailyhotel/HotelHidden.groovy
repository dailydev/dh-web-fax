package kr.dailyhotel

class HotelHidden {

	Hotel hotel
	String name

	static mapping = {
		id column: "idx"
		version false
	}

	static constraints = {
		hotel unique: true
		name nullable: true
	}
}
