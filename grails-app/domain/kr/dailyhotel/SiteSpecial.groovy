package kr.dailyhotel

class SiteSpecial {

	Site site
	Byte siteSpecialCode
	Date regdate
	Date updateDate

	static mapping = {
		id column: "site_idx"
		version false
	}

	static constraints = {
		site unique: true
		siteSpecialCode unique: true
	}
}
