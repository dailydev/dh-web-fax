package kr.dailyhotel

import org.apache.commons.logging.LogFactory

class Hfsenddata {

	private static final log = LogFactory.getLog(this)
	
	String strsenderFaxnum
	String strreceiverFaxnum
	String strsubject
	Integer intconvtif
	String strfilenames
	Integer intfiles
	String strtifnames
	Integer intpages
	Integer intsentpages
	Integer intstate
	String strregdate
	String strreservedate
	String strstarttime
	String strendtime
	String strsid
	String strcountrycode
	Integer intdeleted
	String strresultcode

	def beforeValidate() {
		
		log.debug("beforeValidate")
		
		if (intconvtif == null) {
			intconvtif = 1
		}
		
		if (strregdate == null) {
		   strregdate = new Date().format("yyyy-MM-dd HH:mm:ss")
		}
		
		if (intstate == null) {
			intstate = 0
		}
		
		if(strcountrycode == null) {
			strcountrycode = 82
		}
		
		if (intdeleted == null) {
			intdeleted = 0
		}
		
		if (intfiles == null) {
			intfiles = 1
		}
	 }
	
	static mapping = {
		id column: "intid"
		version false
	}

	static constraints = {
		strsenderFaxnum maxSize: 20
		strreceiverFaxnum maxSize: 20
		strsubject nullable: true, maxSize: 100
		strfilenames maxSize: 800
		strtifnames nullable: true, maxSize: 800
		intpages nullable: true
		intsentpages nullable: true
		strregdate maxSize: 19
		strreservedate nullable: true, maxSize: 19
		strstarttime nullable: true, maxSize: 19
		strendtime nullable: true, maxSize: 19
		strsid maxSize: 30
		strcountrycode maxSize: 5
		strresultcode nullable: true, maxSize: 3
	}
}
