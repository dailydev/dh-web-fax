package kr.dailyhotel

class CodeLang {

	Byte idx
	String name
	String fullName
	Date createDate
	Date updateDate

	static hasMany = [hotelInfos: HotelInfo,
	                  siteNames: SiteName]

	static mapping = {
		id name: "idx", generator: "assigned"
		version false
	}

	static constraints = {
		name maxSize: 2
		fullName maxSize: 45
	}
}
