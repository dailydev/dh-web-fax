package kr.dailyhotel

class Site {

	String name
	Integer parent
	String engname
	Integer seq
	Byte hidden
	SiteSpecial siteSpecial

	static hasMany = [siteNames: SiteName]

	static mapping = {
		id column: "idx"
		version false
	}

	static constraints = {
		name maxSize: 20
		engname nullable: true, maxSize: 20
		hidden nullable: true
		siteSpecial nullable: true, unique: true
	}
}
