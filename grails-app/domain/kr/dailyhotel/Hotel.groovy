package kr.dailyhotel

class Hotel {

	Integer siteidx
	String name
	Double lat
	Double lng
	Integer regionProvinceIdx
	Integer regionDistrictIdx
	Integer price
	Integer discount
	Integer deltaPriceDefault
	String address
	String addrSummary
	String img
	String cat
	Integer seq
	Double rate
	Integer site2idx
	Integer reservAckType
	String email1
	String email2
	String fax1
	String fax2
	String phone1
	String phone2
	String phone3
	Integer hide
	Integer weighting
	Date regdate
	Date updatedate
	String adjustCode
	Integer adjustDiscount

	static hasMany = [hotelInfos: HotelInfo,
	                  roomInfos: RoomInfo]

	static mapping = {
		id column: "idx"
		version false
	}

	static constraints = {
		name maxSize: 200
		price nullable: true
		discount nullable: true
		address maxSize: 300
		addrSummary maxSize: 200
		img maxSize: 200
		cat nullable: true, maxSize: 200
		rate nullable: true
		email1 nullable: true, maxSize: 200
		email2 nullable: true, maxSize: 200
		fax1 nullable: true, maxSize: 45
		fax2 nullable: true, maxSize: 45
		phone1 nullable: true, maxSize: 20
		phone2 nullable: true, maxSize: 20
		phone3 nullable: true, maxSize: 20
		adjustCode maxSize: 1
	}
}
