package kr.dailyhotel

class HotelInfo {

	String name
	String address
	String addrSummary
	String spec
	String way
	Date createDate
	Date updateDate
	CodeLang codeLang
	Hotel hotel

	static belongsTo = [CodeLang, Hotel]

	static mapping = {
		id column: "idx"
		version false
	}

	static constraints = {
		name maxSize: 200
		address maxSize: 300
		addrSummary maxSize: 200
		spec nullable: true, maxSize: 4000
		way nullable: true, maxSize: 4000
	}
}
