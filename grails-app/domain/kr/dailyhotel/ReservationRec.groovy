package kr.dailyhotel

class ReservationRec {

	Integer useridx
	Integer saleidx
	String tid
	String kcpno
	Integer checkin
	Integer bedType
	Integer deposit
	Integer sellingPrice
	Integer deltaPrice
	Integer discount
	Integer faxidx
	Integer fax2idx
	Integer rating
	Boolean userByHidden
	Boolean isCancel
	Integer paymentType
	Date cancelDate
	Date regdate
	Date lastUpdate

	static mapping = {
		id column: "idx"
		version false
	}

	static constraints = {
		tid nullable: true, maxSize: 45
		kcpno maxSize: 50
		faxidx nullable: true
		fax2idx nullable: true
		rating nullable: true
	}
}
