package kr.dailyhotel

class RoomInfo {

	String name
	String detail
	Date regdate
	Integer hide
	Hotel hotel

	static belongsTo = [Hotel]

	static mapping = {
		id column: "idx"
		version false
	}

	static constraints = {
		name maxSize: 200
		detail nullable: true, maxSize: 2000
		hide nullable: true
	}
}
