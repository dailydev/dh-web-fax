package kr.dailyhotel

class Code {

	String type
	String code
	String name
	Integer parent
	Integer seq

	static mapping = {
		id column: "idx", generator: "assigned"
		version false
	}

	static constraints = {
		type maxSize: 45
		code maxSize: 45
		name maxSize: 45
		parent nullable: true
		seq nullable: true
	}
}
