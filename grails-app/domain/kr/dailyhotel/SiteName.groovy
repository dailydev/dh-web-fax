package kr.dailyhotel

class SiteName {

	String name
	Date createDate
	Date updateDate
	CodeLang codeLang
	Site site

	static belongsTo = [CodeLang, Site]

	static mapping = {
		id column: "idx"
		version false
	}

	static constraints = {
		name maxSize: 20
	}
}
