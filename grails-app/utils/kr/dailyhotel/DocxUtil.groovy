package kr.dailyhotel

import javax.xml.bind.JAXBElement
import org.apache.commons.logging.LogFactory
import org.docx4j.openpackaging.packages.WordprocessingMLPackage
import org.docx4j.wml.ContentAccessor
import org.docx4j.wml.Text

class DocxUtil {
	
	private static final log = LogFactory.getLog(this)
	
	public static boolean modifytoSave(def sourcePath, def destinationPath, def params) {
		
		try {
			
			WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(new File(sourcePath));
			List<Object> texts = getAllElementFromObject(wordMLPackage.getMainDocumentPart(), Text.class);
			
			params.each { k, v ->
				
				for (Object text : texts) {
					
					Text textElement = (Text) text;
					def tempValue = "{" + k + "}"
					if (textElement.getValue().equals(tempValue)) {
						
						textElement.setValue(v.toString());
					}
				}
			}
			
			wordMLPackage.save(new File(destinationPath))
		}
		catch (Exception e) {
		
			log.error("DocxUtil.modifytoSave", e)
			return false
		}
		
		return true
	}
	
	// Docx 문서 Element -> Object 변경후 List 반환
	private static List<Object> getAllElementFromObject (Object obj, Class<?> toSearch) {

		List<Object> result = new ArrayList<Object>();

		if (obj instanceof JAXBElement) obj = ((JAXBElement<?>) obj).getValue();

		if (obj.getClass().equals(toSearch)) {
			
			result.add(obj);
		}
		else if (obj instanceof ContentAccessor) {
			
			List<?> children = ((ContentAccessor) obj).getContent();
			for (Object child : children) {
				
				result.addAll(getAllElementFromObject(child, toSearch));
			}
		}
		
		return result
	}
}