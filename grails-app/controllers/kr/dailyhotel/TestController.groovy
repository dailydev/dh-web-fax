package kr.dailyhotel

import grails.converters.JSON
import groovy.time.TimeCategory

class TestController {
	
	def hfsenddataService
	
    def index() {
		
		def reservationRecSingle = ReservationRecSingle.get(274412)
		def hotel = Hotel.get(reservationRecSingle.hotelIdx)
		
		def parameterMap = [:]
		parameterMap.reservIdx = 274412
		parameterMap.paymentDate = reservationRecSingle.regDate.format("yyyy-MM-dd")
		parameterMap.checkIn = reservationRecSingle.checkinDate.format("yyyy-MM-dd")
		parameterMap.checkOut = reservationRecSingle.checkoutDate.format("yyyy-MM-dd")
		
		parameterMap.userN = reservationRecSingle.guestName
		parameterMap.phoneN = reservationRecSingle.guestPhone
		parameterMap.bedType = reservationRecSingle.roomName
		
		parameterMap.depositP = reservationRecSingle.depositTotal
		parameterMap.saleP = reservationRecSingle.sellingPriceTotal
		
		Date startDate = reservationRecSingle.checkinDate
		Date endDate = reservationRecSingle.checkoutDate
		parameterMap.lengthStay = endDate - startDate;
		
		parameterMap.hotelN = hotel.name
		def availableRooms = 0
		while(startDate < endDate) {
			
			def saleReco = SaleReco.findByRoomidxAndSday(reservationRecSingle.roomIdx, startDate)
			def thisAvailableRooms = saleReco.cap - ReservationRec.countBySaleidx(saleReco.id)
			
			if (saleReco.isBlock && hotel.regionProvinceIdx != 5) {
				
				parameterMap.hotelN = "[하드블락]" + hotel.name
			}
			
			if (availableRooms == 0) {
				
				availableRooms = thisAvailableRooms
			}
			else if (availableRooms > thisAvailableRooms) {
				
				availableRooms = thisAvailableRooms
			}
			
			use (TimeCategory) {
				
				startDate = startDate + 1.day
			}
		}
		parameterMap.availableRoom = availableRooms
		
		def createFaxDoc = hfsenddataService.createFaxDoc(parameterMap)
		
		render "test"
	}
}
